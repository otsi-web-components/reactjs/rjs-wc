# OpenTalk Software Initiative Web Components
Opentalk Software Initiative (OtSI) web components is a library of HTML+CSS+JavaScript templates or plugins designed to enable developers quickly deliver on a project by providing high-level abstractions of regularly used web forms, tables, menus etc. 
The collection will include but not limited to the following components:

## Contact Form        
> Display and render a contact form. The options, Minimal (has fields for NAME, EMAIL, NOTE); Standard (has fields for NAME, ADDRESS, MOBILE, EMAIL, NOTE); Detail (has fields for BIODATA, CONTACT ADDR, SERVICE LIST, NOTE). All form data are posted as JSON using REST handle.

## Menu navigation     
> Allow menu navigation with support for sub menus. Menus can be rendered horizontally or vertically.

## Sidebar             
> Allow menu navigation with support for sub menus. Menus is rendered vertically on the Right Hand Side (RHS) or Left Hand Side (LHS)

## Media player        
> Media player for audio or video files or live streaming

## Gallery             
> Display collection of files with option to sort, filter, and view details. Files displayed may be photos, videos, or generic

## Search form         
> A search form with features to allow advance searches. Boolean operators can be added to handle how keywords are combined in searches

## Search results      
> Display a search result as a list or grid view. Options enables results to be displayed with summary and statistics

## User profile        
> Display bio-data and related information about a user. Options allows for viewing basic (NAME, DATE REG, PUBLIC ID) or detail view. Level of detail can be customised using a setting or config file

## Mail box            
> Collection of various mail box components. Available in future edition of web-component library

## Tables              
> Render a table with provided data. Table options defines features such as HEADING, FILTER etc.

## Forms               
> Render a generic form with the provided fields, labels, placeholders, and values

## Shopping Cart       
> Shopping cart plugin and component for ecommerce website. The cart can be displayed in different modes with different functionalities and payment options based on data provided.

## Order Summary       
> Display various level of details for a given order

The design consideration for the OtSI includes:

1. Web components should adhere to conventional UI/UX standards enabling ease of use for average computer users
2. Components should be mobile ready and responsive across a wide range of display units
3. Components should be have clearly defined behaviours (predictable and consistent) across various devices, target web development frameworks, and browsers
4. The library will support various JavaScript template engine and may be slightly altered to fit the general philosophy of the target framework
5. All stylying will be done using Cascading StyleSheets (CSS)
6. Components with nested members may have various configuration settings for both parent and children behaviour and styles
7. Components will consume and return all data as JSON
8. Communication between components and data source will be through clearly defined HTTP request protocols
9. Different design themes will be supported and should be designed to be easily switched or modified from a configuration file
10. Component configuration file can be deployed as JSON or text file with a key-value pair structure. Configuration files should follow a simple key-value structure and nesting of configuration settings should be discouraged

